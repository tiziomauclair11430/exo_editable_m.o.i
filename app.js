let enTete = document.getElementById('en-tete');
let sectionPers = document.getElementById('Person');
let sectionPoint = document.getElementById('Points');
let sectionProduct = document.getElementById('Product');
let validSearch = document.getElementById('input-envoyer');
let search = document.getElementById('input-search');
let aZ = document.getElementById('a-z');
let age = document.getElementById('age');
let triId = document.getElementById('0-10');
let presOption = document.getElementById('pres');
let supp = document.getElementById('supp');
let monTab = [];
let lien = '';
let po = 0;
if(po === 0){
age.style.display = 'none';
aZ.style.display = 'none';
triId.style.display = 'none';
presOption.style.display = 'none';
supp.style.display = 'none';
}
supp.addEventListener('click', function(e) {
	effacer();
	age.style.display = 'none';
	aZ.style.display = 'none';
	triId.style.display = 'none';
	presOption.style.display = 'none';
	supp.style.display = 'none';
});
function sortAge() {
	age.addEventListener('click', function(e) {
		monTab.sort(function(a, b) {
			return a.age - b.age;
		});
		effacer();
		if (po === 1) {
			créerPerson();
		}
	});
	age.addEventListener('dblclick', function(e) {
		monTab.sort(function(a, b) {
			return b.age - a.age;
		});
		effacer();
		if (po === 1) {
			créerPerson();
		}
	});
}
function sortId() {
	triId.addEventListener('click', function(e) {
		monTab.sort(function(a, b) {
			return b.id - a.id;
		});
		effacer();
		if (po === 1) {
			créerPerson();
		}
		if (po === 2) {
			créerProduct();
		}
		if (po === 3) {
			créerPoints();
		}
	});
	triId.addEventListener('dblclick', function(e) {
		monTab.sort(function(a, b) {
			return a.id - b.id;
		});
		effacer();
		if (po === 1) {
			créerPerson();
		}
		if (po === 2) {
			créerProduct();
		}
		if (po === 3) {
			créerPoints();
		}
	});
}
function créerPerson() {
	for (var key in monTab) {
		data = monTab[key];
		sectionPers.innerHTML += `
          <input value="${data.id}" type="button" data-id="${data.id}" data-attr="${data.id} id="idHTML" class="cell">
          <p class="cell-img"><img src="${data.picture}" class="cell-img"></p>
          <input value="${data.age}" type="number" data-id="${data.id}" data-attr="${data.age}" class="cell">
          <input value="${data.name}" type="text" data-id="${data.id}" data-attr="${data.name}" class="cell">    
          <input value="${data.gender}" type="text" data-id="${data.id}" data-attr="${data.gender}" class="cell">
          <input value="${data.about}" type="text" data-id="${data.id}" data-attr="${data.about}" class="cell">
          <input value="${data.registered}" type="text" data-id="${data.id}" data-attr="${data.registered}" class="cell">
          `;
	}
	for (var key of Object.keys(data)) {
		enTete.innerHTML += `
          <p class="key">${key}</p>`;
	}
}
function créerProduct() {
	for (var key in monTab) {
		data = monTab[key];
		sectionProduct.innerHTML += `
        <input value="${data.id}" type="button" data-id="${data.id}" data-attr="${data.id}" class="cell">
        <input value="${data.price}" type="number" data-id="${data.id}" data-attr="${data.price}" class="cell">
        <input value="${data.name}" type="text" data-id="${data.id}" data-attr="${data.name}" class="cell">
        <input value="${data.available}" type="bool" data-id="${data.id}" data-attr="${data.available}" class="cell">
        <ul class="cell"><li>${data.tags}</li></ul>`;
	}
	for (var key of Object.keys(data)) {
		console.log(Object.entries(data));
		enTete.innerHTML += `
        <p class="key">${key}</p>`;
	}
}
function créerPoints() {
	for (var key in monTab) {
		data = monTab[key];
		sectionPoint.innerHTML += `
       <input value="${data.id}" type="button" class="cell">
       <input value="${data.x}" type="number" class="cell">
       <input value="${data.y}" type="number" class="cell">
       <input value="${data.z}" type="number" class="cell">`;
	}
	for (var key of Object.keys(data)) {
		console.log(Object.entries(data));
		enTete.innerHTML += `
        <p class="key">${key}</p>`;
	}
}
function effacer() {
	sectionPoint.innerHTML = '';
	sectionProduct.innerHTML = '';
	sectionPers.innerHTML = '';
	enTete.innerHTML = '';
}
function aZ_name() {
	aZ.addEventListener('click', function(e) {
		effacer();
		maClef = 'name';
		monTab.sort((a, b) => a[maClef].toString().localeCompare(b[maClef].toString()));
		console.log(monTab);
		if (po === 1) {
			créerPerson();
		} else if (po === 2) {
			créerProduct();
		}
	});

	aZ.addEventListener('dblclick', function(e) {
		effacer();
		maClef = 'name';
		monTab.sort((a, b) => b[maClef].toString().localeCompare(a[maClef].toString()));
		console.log(monTab);
		if (po === 1) {
			créerPerson();
		} else if (po === 2) {
			créerProduct();
		}
	});
}
let perso_json = document.getElementById('Perso');
let produc_json = document.getElementById('Produc');
let points_json = document.getElementById('Point');

perso_json.addEventListener('click', function(e) {
	fetch('./data/person.json') // j'envoie ma requête
		.then((reponse) => {
			console.log(reponse);

			if (reponse.ok) {
				reponse.json().then((data) => {
					monTab = data;
					po = 1;
					effacer();
					créerPerson();
					aZ_name();
					sortAge();
					sortId();
					if (po === 1) {
						age.style.display = '';
						aZ.style.display = '';
						triId.style.display = '';
						presOption.style.display = '';
						supp.style.display = '';
					}
				});
			}
		});
});
produc_json.addEventListener('click', function(e) {
	fetch('./data/product.json') // j'envoie ma requête
		.then((reponse) => {
			console.log(reponse);

			if (reponse.ok) {
				reponse.json().then((data) => {
					monTab = data;
					po = 2;
					effacer();
					créerProduct();
					aZ_name();
					sortId();
					if (po === 2) {
						age.style.display = 'none';
            presOption.style.display = '';
            triId.style.display = '';
            supp.style.display = '';
						aZ.style.display = '';
					}
				});
			}
		});
});

points_json.addEventListener('click', function(e) {
	fetch('./data/points.json') // j'envoie ma requête
		.then((reponse) => {
			console.log(reponse);

			if (reponse.ok) {
				reponse.json().then((data) => {
					monTab = data;
					po = 3;
					effacer();
					créerPoints();
					sortId();
					if (po === 3) {
						age.style.display = 'none';
						aZ.style.display = 'none';
            triId.style.display = '';
            presOption.style.display = '';
            supp.style.display = '';
					}
				});
			}
		});
});

let data
function getJson(){
  let url = ""
  fetch(url)
  .then(
    rep =>rep.json()
  )
  .then(
    json_data => data = json_data
  )
}